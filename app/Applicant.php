<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Job;
use App\User;


class Applicant extends Model {

	protected $guarded = [];

	protected $dates = [];

	public static $rules = [
        // Validation rules
	];

    // Relationships

	public function job()
	{
		return $this->belongsTo('App\Job');
	}


	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
