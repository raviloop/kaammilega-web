<?php
 namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;


class Experience extends Model {

    protected $guarded = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships


    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
