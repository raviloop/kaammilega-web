<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

use App\User;
use DB;

class Controller extends BaseController
{
    //

    public function demo()
    {
    	return view('demo');
    }

    public function sendData()
    {
    	$users = DB::table('users')->get();
    	return json_encode($users);
    }

    public function demoPagination($pageNumber,$showThis)
    {
    	$show = (int)$pageNumber - 1;
    	$show*= (int)$showThis;
    	$users = DB::table('users')->skip($show)->take((int)$showThis)->get();
    	return json_encode($users);
    }

    public function return_xml()
    {
        $data =User::all();

        //dd(json_encode($data));
         // $content = view('product.xml', compact('products'));
         return response($data, 200)->header('Content-Type', 'text/xml');

    }

}
