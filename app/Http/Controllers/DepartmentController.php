<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Department;

class DepartmentController extends Controller
{
    public function all_department()
    {
    	return Department::get();
    }

    public function datatable_department()
    {
        $departments=[];
        $departments['data']=Department::select('department')->get();
        return  $departments;
    }

    public function add_department(Request $request)
    {
        $department = new Department;
         $result;
        if($request['depart'] == "")
        {
            $result = "Department_blank";
        }
    	
    	else
        {
       
        $number = Department::where('department',$request['depart'])->count();
       
        if($number>0) 
        {
           
            $result = "Department_yes";
        }
        else
        {
           
            Department::create(['department'=>$request['depart']]);
            $result = "Department_added" ; 
        }
    }


        return $result;
    }

    public function rename_department(Request $request)
    {
        $result;
        if($request['new'] == "")
        {
            $result = "Department_blank";
        }
        
        else
        {
    	
        $count = Department::where('department',$request['new'])->count();
        if($count>0) 
        {
            $result = "department_already";
        }
        else
        {

           Department::where('department','=',$request['old'])->update(['department'=>$request['new']]);
           
            $result = "department_rename" ; 
        }
    }

        return $result;

    }

    public function remove_department(Request $request)
    {
    	Department::where('department',$request['depart'])->delete();
    	return "department_deleted";
    }
}