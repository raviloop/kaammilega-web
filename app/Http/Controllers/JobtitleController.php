<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Jobtitle;

class JobtitleController extends Controller
{
    public function all_jobstitles()
    {
    	return Jobtitle::get();
    }

    public function datatable_jobtitles()
    {
        $jobs=[];
        $jobs['data']=Jobtitle::select('job_title')->get();

        return $jobs;
    }
    public function add_jobtitle(Request $request)
    {
    	$jobtitle = new Jobtitle;
        $result;
        if($request['title'] == "")
        {
            $result = "job_title_blank";
        }

        else{

            $number = Jobtitle::where('job_title',$request['title'])->count();

            if($number>0) 
            {

                $result = "jobtitle_yes";
            }
            else
            {

                Jobtitle::create(['job_title'=>$request['title']]);
                $result = "jobtitle_added" ; 
            }

        }
        return $result;
    }

    public function rename_jobtitle(Request $request)
    {
        $jobtitle = new Jobtitle;
        
        $result;
        if($request['new'] == "")
        {
            $result = "job_title_blank";
        }

        else{
            
            $count = $jobtitle->where('job_title',$request['new'])->count();
            if($count>0) 
            {
                $result = "jobtitle_already";
            }
            else
            {

                Jobtitle::where('job_title','=',$request['old'])->update(['job_title'=>$request['new']]);

                $result = "jobtitle_rename" ; 
            }

        }
        return $result;


    }

    public function remove_jobtitle(Request $request)
    {
       Jobtitle::where('job_title',$request['title'])->delete();
       return "Jobtitle_deleted";
   }
}