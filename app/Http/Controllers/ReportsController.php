<?php

namespace App\Http\Controllers;

use App\Report;
use App\User;
use App\Posting;
use App\Common;
use App\Job;
use App\Shop;
use App\Status;
use App\Applicant;
use App\Pincode;
use App\Date;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class ReportsController extends Controller
{
    public function updateReport()
    {
        $start_date=Report::pluck('report_on_date')->first();
        // $start_date="2016-08-31 00:00:00";
        $today=Carbon::today();
        $yesterday=Carbon::yesterday();

        $postings=Posting::where('updated_at','>=',$start_date)->where('job_id','!=',0)->get();
        $applicants=Applicant::where('updated_at','>=',$start_date)->get();

        $all_common=Common::all();
        
            //To check new Postings
                foreach($postings as $post)
                {
                    // return $post;
                    if(count(Common::where('retailer_id',$post->user_id)->where('job_id',$post->job_id)->where('shop_id',$post->shop_id)->get())>0)
                    {
                        $status_id=Status::where('status',$post->status)->pluck('id')->first();
                        Common::where('retailer_id',$post->id)->
                        where('job_id',$post->job_id)->
                        where('shop_id',$post->shop_id)->
                        update([
                            'job_status' => $status_id
                        ]);
                    }
                    else
                    {
                        $this->insert($post,$start_date);
                    }
                }

                foreach($applicants as $applicant)
                {
                    if(Common::where('job_id',$applicant->job_id)->where('applicant_id',$applicant->user_id)->get())
                    {
                        $status_id=Status::where('status',$applicant->status)->pluck('id')->first();
                        Common::where('job_id',$applicant->job_id)->
                                where('applicant_id',$applicant->user_id)->
                                update([
                                    'applicant_status' => $status_id
                                ]);
                    }

                    else
                    {
                        $post=Posting::where('job_id',$applicant->job_id)->get();
                        $this->insert_applicant($post,$applicant->user_id);
                    }

                }

        //Update Report
        Report::where('report_on_date',$start_date)->update([
            'report_of_date' => $yesterday,
            'report_on_date' => $today,
            'status' => 'updated'
        ]);

        return "Updated";

    }

    
    public function insert($posting,$start_date)
    {
        // dd($posting);
        $applicants=Applicant::where('updated_at','>=',$start_date)->where('job_id',$posting->job_id)->pluck('user_id');
        foreach($applicants as $applicant)
        {
            $this->insert_applicant($posting,$applicant);
        }
    }

    public function insert_applicant($posting,$applicant)
    {
        $common=new Common;
        $common->retailer_id=$posting->user_id;
        $common->shop_id=$posting->shop_id;
        $common->job_id=$posting->job_id;

        $job_status=Status::where('status',$posting->status)->pluck('id')->first();
        $common->job_status=$job_status;

        $retailer_reg_date=User::where('id',$common->retailer_id)->pluck('created_at')->first();
        $common->retailer_reg_date=Date::where('date',$retailer_reg_date->toDateString())->pluck('date_id')->first();
        
        $shop_reg_date=Shop::where('id',$common->shop_id)->pluck('created_at')->first();
        $common->shop_reg_date=Date::where('date',$shop_reg_date->toDateString())->pluck('date_id')->first();
        
        $job_posted_date=Job::where('id',$common->job_id)->pluck('created_at')->first();
        $common->job_posted_date=Date::where('date',$job_posted_date->toDateString())->pluck('date_id')->first();
        
        $retailer_pincode=User::where('id',$common->retailer_id)->pluck('pincode')->first();
        if(count($retailer_pincode)<1)
        {
            $retailer_pincode=0;
        }
        $retailer_pin=Pincode::where('pincode',$retailer_pincode)->pluck('id')->first();
        $common->retailer_pincode=0;
        if($retailer_pin!=0)
        {
            $common->retailer_pincode=$retailer_pin;
        }

        $shop_pincode=Shop::where('id',$common->shop_id)->pluck('pincode')->first();
        if(count($shop_pincode)<1)
        {
            $shop_pincode=0;
        }
        $shop_pin=Pincode::where('pincode',$shop_pincode)->pluck('id')->first();
        $common->shop_pincode=0;
        if($shop_pin!=0)
        {
            $common->shop_pincode=$shop_pin;
        }

        //Job Closing Date
        if($common->job_status=="closed")
        {
            $job_closed_date=Job::where('id',$common->job_id)->pluck('updated_at')->first();
            $common->job_closed_date=Date::where('date',$job_closed_date->toDateString())->pluck('date_id')->first();
        }
        else
        {
            $common->job_closed_date=Date::where('date',"1990-01-01")->pluck('date_id')->first();       
        }

        $common->applicant_id=$applicant;

        $applicant_reg_date=User::where('id',$applicant)->pluck('created_at')->first();
        $common->applicant_reg_date=Date::where('date',$applicant_reg_date->toDateString())->pluck('date_id')->first();

        $applicant_pincode=User::where('id',$applicant)->pluck('pincode')->first();
        if(count($applicant_pincode)<1)
        {
            $applicant_pincode=0;
        }
        $applicant_pin=Pincode::where('pincode',$applicant_pincode)->pluck('id')->first();
        $common->applicant_pincode=0;
        if($applicant_pin!=0)
        {
            $common->applicant_pincode=$applicant_pin;
        }

        $applicant_status=Applicant::where('user_id',$applicant)->pluck('status')->first();
        $applicant_status_id=Status::where('status',$applicant_status)->pluck('id')->first();
        $common->applicant_status=$applicant_status_id;
        
        $common->save();

    }

    public function all()
    {
        $reports=Common::all();

        return $reports;
    }

    public function stats()
    {
        $types = [
            "Retailer",
            "Shops",
            "Job Type",
            "Job Title",
            "Department",
            // "Salary Basis",
            // "Job Status",
            // "Applicant Status",
            // "Shop Location",
            // "Retailer Location",
            // "Applicant Locations"
        ];

        $range = [
            "Day On Day",
            "Weekday vs Weekend",
            "Week On Week",
            "Month On Month",
            "Quarter On Quarter",
            "Year On Year"
        ];

        $conditions = [
            "None",
            "Day",
            "Week",
            "Month"
        ];

        $final_data['types']=$types;
        $final_data['range']=$range;
        $final_data['condition']=$conditions;

        
        // $values=DB::table('common')
        // ->join('users','users.id','=','common.retailer_id')
        // ->join('date_dim','date_dim.date_id','=','common.retailer_reg_date')
        // ->select('date_dim.day_name as Day',DB::raw('count(distinct common.retailer_id) as Retailers'))
        // ->groupBy('Day')
        // ->get();


        // return $values;

        return $final_data;
    }

    public function sendStats(Request $request)
    {
        $types=$request['type'];
        $range=$request['range'];
        $condition=$request['condition'];
        $start_date=$request['start_date'];
        $end_date=$request['end_date'];

        $types=preg_split('/, /',$types);

        // return $condition;

        // $final_data=array();
        // foreach($types as $type)
        // {
        //     if ($type == "Retailer" && $range == "Day On Day" && $condition == "None")
        //     {
        //         $values=DB::table('common')
        //         ->join('users','users.id','=','common.retailer_id')
        //         ->join('date_dim','date_dim.date_id','=','common.retailer_reg_date')
        //         ->select('date_dim.day_name as Day',DB::raw('count(distinct common.retailer_id) as Retailers'))
        //         ->groupBy('Day')
        //         ->get();

        //         foreach($values as $value)
        //         {
        //             array_push($final_data,$value);
        //         }
        //     }
        // }

        $final_data=DB::select(DB::raw('select users.name as Retailer_name, shops.name as Shop_Name, 
        retailer_regdt.week_starting_monday as Retailer_Reg_DT, shop_regdt.week_starting_monday as Shop_Reg_DT,
        applicant_regdt.week_starting_monday as Applicant_Reg_Dt,
        count(distinct common.retailer_id) as retailer_count, count(distinct common.shop_id) as shop_count,
        count(common.applicant_id) as applicant_count
        from kaammilega.users, kaammilega.common, kaammilega.date_dim as retailer_regdt,kaammilega.date_dim as shop_regdt,kaammilega.shops,
        kaammilega.users as applicants, kaammilega.date_dim as applicant_regdt
        where users.id = common.retailer_id
        and shops.id= common.shop_id
        and retailer_regdt.date_id = common.retailer_reg_date
        and shop_regdt.date_id = common.shop_reg_date
        and applicants.id	= common.applicant_id 
        and applicant_regdt.date_id = common.applicant_reg_date
        group by users.name, shops.name,retailer_regdt.week_starting_monday, shop_regdt.week_starting_monday,applicant_regdt.week_starting_monday'));
        return $final_data;
        // return DB::select(DB::raw("select * from common where x='$variable' limit 10"));
        
    }

    public function check(Request $request)
    {
        return Common::all();
    }
}