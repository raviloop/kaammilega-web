<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Applicant;
use App\Posting;
use App\User;
use App\Job;
use App\Image;
use App\Shop;
use App\Jobtitle;
use App\Department;
use App\RequestCheck;
use App\Experience;

class RequestController extends Controller
{


	public function sendRequest(Request $request)
	{	
		$rId = $request['retailerId'];
		$sId = $request['salesmanId'];
		$jId = $request['jobId'];


        $isRequested = RequestCheck::where('retailer_id',$rId)->where('salesman_id',$sId)->where('job_id',$jId)->get();

        $isApplicant = Applicant::where('user_id',$sId)->where('job_id',$jId)->get();

        if(count($isApplicant) > 0 || count($isRequested) > 0){
            return "Already Requested";
        }
        else{


            $request = new RequestCheck;

            $request->retailer_id = $rId; 
            $request->salesman_id = $sId;
            $request->job_id = $jId;

            $request->save();

            $retailername = User::where('id',$rId)->pluck('name')->first();
            $jobtitle =Jobtitle::where('id',Job::where('id',$jId)->pluck('title')->first())->pluck('job_title')->first(); 

            $this->sendRequestNotify($retailername,$jobtitle,$sId,$jId,$rId);

            return "Request Sent";

        }

    }

    function sendRequestNotify($rname,$title,$sId,$jId,$rId){

        $msg = $rname." requested for ".$title." job";

        $content = array(
            "en" => $msg
            );

        $device = array();
        array_push($device,User::where('id',$sId)->pluck('device')->first());
        // dd($device);
        $fields = array(
            'app_id' => "31e759f6-a64f-44ea-81e1-ee32e49d019c",
            'include_player_ids' => $device,
            'data' => array("job_id"=>$jId,"retailer_id"=>$rId,"type"=>"requestsalesman","msg"=>$msg),
            'contents' => $content
            );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ZjgyNzU1NDYtMWRhOC00ZWMyLWI4NTItMjUyOGQyZjk4MjYz'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);


    }



    public function responseFromSalesman(Request $request)
    {
        $response = $request['response'];
        $jobId = $request['job_id'];
        $retailerId = $request['retailer_id'];
        $salesmanId = $request['salesman_id'];

        if($response == 'accepted'){

           $request = new RequestCheck;
           $request->retailer_id = $retailerId;
           $request->salesman_id = $salesmanId;
           $request->job_id = $jobId;
           $request->status = $response;

           $request->save();

           $applicant = new Applicant; 
           $applicant->user_id = $salesmanId;
           $applicant->job_id = $jobId;
           $applicant->status = 'applied';

           $applicant->save();

           return 'Respose Submitted';

       }
       else if($response == 'rejected'){

           $request = new RequestCheck;
           $request->retailer_id = $retailerId;
           $request->salesman_id = $salesmanId;
           $request->job_id = $jobId;
           $request->status = $response;

           $request->save();

           return "Response Submitted";
       }
       else{
        return "Response not matched";
    }


}

public function getAllRequestSalesman($rId)
{

    $requests = RequestCheck::select('salesman_id','job_id','status')->where('retailer_id',$rId)->get();

    if(count($requests) > 0){

        foreach ($requests as $req) {
            $req['job_title'] = Jobtitle::where('id',Job::where('id',$req['job_id'])->pluck('title')->first())->pluck('job_title')->first();
            $req['name'] = User::where('id',$req['salesman_id'])->pluck('name')->first();
            $req['mobile'] = User::where('id',$req['salesman_id'])->pluck('mobile')->first();
            $req['address'] = User::where('id',$req['salesman_id'])->pluck('address')->first();
            $req['address2'] = User::where('id',$req['salesman_id'])->pluck('address2')->first();
            $req['city'] = User::where('id',$req['salesman_id'])->pluck('city')->first();
            $req['state'] = User::where('id',$req['salesman_id'])->pluck('state')->first();
            $req['image'] = Image::where('user_id',$req['salesman_id'])->pluck('url')->first();
        }

        return $requests;
        
    }

    else{

        return 'noSalesman';
    }

}


public function allJobRequested($sId)
{
    
    $job_ids = RequestCheck::where('salesman_id',$sId)->where('status','pending')->pluck('job_id');

        // return $job_ids;
            if(count($job_ids)>0)  // return $job_ids;
            {
                foreach ($job_ids as $job_id) 
                {
                    $jobs[] = Job::find($job_id);
                }

                for ($i=0; $i <count($jobs) ; $i++) 
                { 

                    $jobs_shopid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('shop_id');
                    $jobs_retailerid=Posting::where('job_Id','=',$jobs[$i]['id'])->pluck('user_id');
                    $jobs[$i]['retailer']=User::where('id',$jobs_retailerid)->select('id','type',
                        'name','email','gender','dob','describe_yourself','salary_basis','mobile','address')->first();
                    $jobs[$i]['shop']=Shop::where('id',$jobs_shopid)->select('id','name','department','contact','address','working_hours','established_year')->first();
                    $jobs[$i]['shop']['department'] = Department::where('id',$jobs[$i]['shop']->department)->pluck('department')->first();
                    $jobs[$i]['title'] = Jobtitle::where('id',$jobs[$i]->title)->pluck('job_title')->first();
                    $jobs[$i]['status'] = RequestCheck::where('salesman_id',$sId)->where('job_Id','=',$jobs[$i]['id'])->pluck('status')->first();


                    $image_count=count(Image::where('shop_id',$jobs_shopid)->pluck('url'));

                    if($image_count>0)
                    {
                        $jobs[$i]['shop']['images']=Image::where('shop_id',$jobs_shopid)->pluck('url');
                    }


                }

                return $jobs;
            }
            else
            {

                return "no_jobs";
            }


        }


    }