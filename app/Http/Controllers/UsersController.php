<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Experience;
use App\User;
use App\Jobtitle;
use App\Job;
use App\Image;
use App\Department;
use App\Posting;

class UsersController extends Controller {


	public function allSalesman(){

		$salesman = User::where('type','salesman')->get();

		$image = new Image;

		foreach ($salesman as $saleperson) {
			$salesman_id = $saleperson->id;
			$saleperson['image'] = $image->where('user_id',$salesman_id)->pluck('url')->first();
			$department_id = Experience::where('user_id',$salesman_id)->pluck('department')->all();
			$saleperson['departments'] = Department::whereIn('id',$department_id)->pluck('department')->all();
		}



		$final = [];
		$final['salesman'] = $salesman;
		$final['departments'] = Department::pluck('department')->all();

		return $final;


	}

	public function premium_salesman()
	{

		$salesman = User::where('type','salesman')->orderBy('created_at','desc')->limit(5)->get();

		// return $s;

		// $salesman = User::where('type','salesman')->get();

		$image = new Image;

		foreach ($salesman as $saleperson) {
			$salesman_id = $saleperson->id;
			$saleperson['image'] = $image->where('user_id',$salesman_id)->pluck('url')->first();
			$department_id = Experience::where('user_id',$salesman_id)->pluck('department')->all();
			$saleperson['departments'] = Department::whereIn('id',$department_id)->pluck('department')->all();
		}



		return $salesman;
	}

	public function allRetailer()
	{
		
		return User::where('type','retailer')->get();

	}

	public function get($id)
	{
		$user = User::find($id);

		if($user->type == "retailer")
		{
			return $user;
		}
		else
		{
			$total_exp = Experience::where('user_id',$user->id)->pluck('years');
			if(count($total_exp)>0)
			{
				$sum_exp=0;
				foreach ($total_exp as $exp) {

					$sum_exp += $exp;
				}
				$image = new Image;
				$user['image'] = $image->where('user_id',$id)->pluck('url')->first();
				$user['experience']= Experience::where('user_id',$user->id)->orderBy('to_year','desc')->first();
				$user['experience']['department'] = Department::where('id',$user['experience']['department'])->pluck('department')->first();

				$user['total']=$sum_exp;
				return $user;
			}
			else
			{
				$image = new Image;
				$user['image'] = $image->where('user_id',$id)->pluck('url')->first();
				$user['experience']="0";
				$user['total']="fresher";
				return $user;
			}

		}
		

		

	}

	public function add(Request $request){


		$user = new User;

		if($request['type'] == 'retailer')
		{

			$user->type = 'retailer';
			$user->name = $request['name'];
			$user->email = $request['email'];
			$user->password = Hash::make($request['password']);
			$user->mobile= $request['mobile'];
			$user->address = $request['address'];
			$user->address2 = $request['address2'];
			$user->pincode = $request['pincode'];
			$user->city = $request['city'];
			$user->state = $request['state'];
			$user->pincode = $request['pincode'];
			// $user->country= $request['country'];
			$user->gender= $request['gender'];
			$user->dob= $request['dob'];
			$user->describe_yourself= 'r';
			$user->salary_basis= 'r';

			$user->save();	

			return $user->id;
		}

		else if ( $request['type'] == 'salesman'){


			$user->type = 'salesman';
			$user->name = $request['name'];
			$user->email = $request['email'];
			$user->password = Hash::make($request['password']);
			$user->mobile= $request['mobile'];
			$user->city = $request['city'];
			$user->state = $request['state'];
			$user->pincode = $request['pincode'];
			$user->address = $request['address'];
			$user->address2 = $request['address2'];
			// $user->country= $request['country'];
			$user->gender= $request['gender'];
			$user->dob= $request['dob'];
			$user->describe_yourself= $request['describe_yourself'];
			$user->salary_basis= $request['salary_basis'];

			$user->save();	

			$image = new Image;
			$image->url = $request['salesmanImg'];
			$image->user_id = $user->id;

			$image->save();

			return $user->id;

		}

		else {

			return "Type not matched";
		}

	}




	public function put(Request $request, $id){


		User::where('id',$id)->update($request->all());	

		return "User Updated";


	}

	public function remove($id){

		$user = User::find($id);

		$user->delete();

		return "User deleted";

	}

	public function update_user(Request $request)
	{
		
		User::where('id',$request['id'])->update($request->all());
		return "User Updated";
	}

	public function allJobs($rId)
	{
		$jobs = [];


		$job_ids = Posting::where('user_id',$rId)->where('job_id','!=',0)->pluck('job_id');

		$jobtitle_ids = Job::whereIn('id',$job_ids)->pluck('title');

		foreach ($job_ids as $id) {
			$title_id = Job::where('id',$id)->pluck('title')->first();
			$jobtitle = Jobtitle::where('id',$title_id)->pluck('job_title')->first();
			$job = [];
			$job['title'] = $jobtitle;
			$job['id'] = $id;

			array_push($jobs,$job);  
		}

		return $jobs;

	}

}
