<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Shop;
use App\User;

class Image extends Model {

    protected $guarded = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    public function shop()
    {
    	return $this->belongsTo('App\Shop');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
