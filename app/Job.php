<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Posting;
use App\Applicant;

class Job extends Model {

    protected $guarded = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

    public function postings()
    {
    	return $this->hasMany('App\Posting');
    }


    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }

}
