<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Pincode extends Model {

    protected $table = 'pincodes';

    protected $guarded = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];
}
