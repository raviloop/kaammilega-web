<?php
 namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Shop;
use App\Job;
use App\User;

class Posting extends Model {

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships


	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function shop()
	{
		return $this->belongsTo('App\Shop');
	}


	public function job()
	{
		return $this->belongsTo('App\Job');
	}

}
