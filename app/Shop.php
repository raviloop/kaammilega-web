<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Image;
use App\Posting;



class Shop extends Model {

    protected $guarded = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships

    public function postings(){
    	return $this->hasMany('App\Posting');
    }

     public function images(){
    	return $this->hasMany('App\Image');
    }

}
