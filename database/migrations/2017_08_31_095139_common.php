<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Common extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common', function (Blueprint $table) {
            $table->integer('retailer_id');
            $table->integer('shop_id');
            $table->integer('job_id');
            $table->integer('applicant_id');
            $table->integer('job_status');
            $table->integer('user_type');
            $table->integer('retailer_pincode');
            $table->integer('applicant_status');
            $table->integer('shop_pincode');
            $table->integer('applicant_pincode');
            $table->dateTime('retailer_reg_date');
            $table->dateTime('shop_reg_date');
            $table->dateTime('applicant_reg_date');
            $table->dateTime('job_posted_date');
            $table->dateTime('job_closed_date');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
