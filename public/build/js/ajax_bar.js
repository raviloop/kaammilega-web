$(document).ready(function(){

    var result = sessionStorage.getItem("checkk");
    if(result)
    {
        dataload();
    }
     else
    {
            $(location).attr('href','login.html');
    }

});

var baseUrl="ec2-13-126-220-182.ap-south-1.compute.amazonaws.com/";
//var baseUrl="http://localhost:8000/";
function dataload()
{

    var our_Request = new XMLHttpRequest();

    our_Request.onload=function()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var our_Data=JSON.parse(our_Request.responseText);
           
            console.log("ajax call succeeded");



            document.getElementById("retailer_header").innerHTML=our_Data.retailer;
            document.getElementById("applicant_header").innerHTML=our_Data.applicant;
            document.getElementById("jobs_header").innerHTML=our_Data.jobs;
            document.getElementById("placement_header").innerHTML=our_Data.placement;
            document.getElementById("salesman_header").innerHTML=our_Data.salesman;
            document.getElementById("vacancies_header").innerHTML=our_Data.vacancies.open_position;

            document.getElementById("retailer_per").innerHTML=our_Data.retailer_per+"%";
            document.getElementById("salesman_per").innerHTML=our_Data.salesman_per+"%";
            document.getElementById("jobs_per").innerHTML=our_Data.jobs_per+"%";
            document.getElementById("placement_per").innerHTML=our_Data.placement_per+"%";
            document.getElementById("applicant_per").innerHTML=our_Data.applicant_per+"%";
            document.getElementById("vacancies_per").innerHTML=our_Data.vacancies_per+"%";
            
            var month_name=[];
            var bar1_value=[];
            var bar2_value=[];
            var line_value=[];
            var salary_typ=[];
            var salary_count=[];
            var job_type_pie_name=[];
            var job_title_pie_name=[];
            var job_type_count=[];
            var job_title_count=[];

            var calc1=[];
            var jty_pg=[];
            var jt_pg=[];


            for(var i =0 ;i<5 ; i++)
            {
                var max1=0,max2=0;

                max1=our_Data.job_type[0].count_type;
                max2=our_Data.job_title[0].count_title;
                max1 = max1 * 1.1;
                max2 = max2 * 1.1;
                jty_pg[i]=Math.floor((our_Data.job_type[i].count_type/max1)*100);
                jt_pg[i]=Math.floor((our_Data.job_type[i].count_title/max2)*100);

                

            }

            for(var i =0 ;i<5 ; i++)
            {
                 document.getElementById("jty"+i+"").innerHTML=our_Data.job_type[i].job_type;

                 document.getElementById("jtyc"+i+"").innerHTML=our_Data.job_type[i].count_type;

                document.getElementById("jtyp"+i+"").style.width=jty_pg[i]+"%";             

                 document.getElementById("jt"+i+"").innerHTML=our_Data.job_title[i].title;

                 document.getElementById("jtp"+i+"").style.width=jt_pg[i]+"%";

                 document.getElementById("jtc"+i+"").innerHTML=our_Data.job_title[i].count_title;
            }

            for (var i in our_Data.months) 
            {
            month_name.push(our_Data.months[i]);
            }
            for(var i in our_Data.no_of_jobs)
            {
                bar1_value.push(our_Data.no_of_jobs[i]);
            }
            for(var i in our_Data.no_of_applicants)
            {
                bar2_value.push(our_Data.no_of_applicants[i]);
            }
            for(var i in our_Data.no_of_placements)
            {
                line_value.push(our_Data.no_of_placements[i]);
            }
            for(var i in our_Data.salary_basis)
            {
                salary_typ.push(our_Data.salary_basis[i].salary_basis);
                salary_count.push(our_Data.salary_basis[i].count_type);
            }

            for(var i in our_Data.job_type_basis)
            {
                job_type_pie_name.push(our_Data.job_type_basis[i].job_type);
                job_type_count.push(our_Data.job_type_basis[i].count_type);
            }

            for(var i in our_Data.job_title_basis)
            {
                job_title_pie_name.push(our_Data.job_title_basis[i].title);
                job_title_count.push(our_Data.job_title_basis[i].count_type);
            }

  
              makegraph(month_name,bar1_value,bar2_value,line_value);
              makepie1(salary_typ,salary_count);
              makepie2(job_type_pie_name,job_type_count);
              // makepie3(job_title_pie_name,job_title_count);
        }

    }

    our_Request.open('POST',baseUrl+'adminpanel');
    our_Request.send();


    function makegraph(month,bar1,bar2,line)
    {
       var ctx = document.getElementById("mycanvas");
       var lab = new Array();
       var colr = new Array();

       var barChartData =
       {
        labels:month,
        datasets:[
        {
            label: "No of Jobs",
            data:bar1,
            backgroundColor:"rgba(32,178,170, 0.5)",
            borderColor:"rgba(32,178,170, 1)",
            hoverbackgroundColor:"rgba(255, 206, 86,1)",
            hoverborderColor:"rgba(255, 206, 86,1)",
            borderWidth: 1

        },

        {
            label: "No of Applicants",
            data:bar2,
            backgroundColor:"rgba(255, 159, 64, 0.4)",
            borderColor:"rgba(255, 159, 64, 1)",
            hoverbackgroundColor:"rgba(255, 159, 64, 1)",
            hoverborderColor:"rgba(255, 159, 64, 1)",
            borderWidth: 1

        },
        {
            type:'line',
            label: "No of placements",
            data:line,
            fill:false,
            lineTension:0.1,
            backgroundColor:"rgba(153, 102, 255, 0.75)",
            borderColor:"rgba(153, 102, 255,1)",
            pointHoverBackgroundColor:"rgba(153, 102, 255, 1)",
            pointHoverBorderColor:"rgba(255, 206, 86,1)"


        }
        ]

        };
        for(var i in barChartData.datasets)
        {
            colr[i]=barChartData.datasets[i].backgroundColor;
            lab[i]=barChartData.datasets[i].label;
        }

        for (var j = 0; j < (lab.length); j++)
        {
            var colo = document.createElement("TD");

            colo.style.backgroundColor =  colr[j];
            colo.style.padding= "10px";
            document.getElementById("myTr").appendChild(colo);

            var labe = document.createElement("TD");

            var txt = document.createTextNode(lab[j]);

            labe.appendChild(txt);
            labe.style.padding = ".2em";

            document.getElementById("myTr").appendChild(labe);
         }

        var barChart = new Chart(ctx,
        {
            type:'bar',
            data: barChartData

        });

         

    }

    function makepie1(salary_typ,salary_count)
    {
        
        var ctp = document.getElementById("mypie1");
        var pieChartData=
        {
            labels:salary_typ,
            datasets:[
            {
                label: "Types of Salary",
                data:salary_count,
                backgroundColor:[
                "rgba(255,178,170, 0.5)",
                
                "rgba(153, 102, 255, 0.5)"
                ],
                borderColor:[
                "rgba(255,255,255,0.9)",
               
                "rgba(255,255,255,0.9)"],
                borderWidth: [3,3,3]

            }
            ]
        };
            var pieChart1 = new Chart(ctp,
        {
            type:'pie',
            data:pieChartData
            
        });
    }
    function makepie2(job_typ,job_count)
    {
        
        var ctp = document.getElementById("mypie2");
        var pieChartData=
        {
            labels:job_typ,
            datasets:[
            {
                label: "Types of Salary",
                data:job_count,
                backgroundColor:[
                "rgba(255,178,170, 0.5)",
                
                "rgba(153, 102, 255, 0.5)"
                ],
                borderColor:[
                "rgba(255,255,255,0.9)",
               
                "rgba(255,255,255,0.9)"],
                borderWidth: [3,3,3]

            }
            ]
        };
            var pieChart2 = new Chart(ctp,
        {
            type:'pie',
            data:pieChartData
            
        });
    }
    
}



