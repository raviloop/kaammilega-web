$(document).ready(function(){

	datatable_jobtitle();
	ajax_jobtitle();
	$('.ui-pnotify').remove();
});


var baseUrl="http://ec2-13-126-220-182.ap-south-1.compute.amazonaws.com/";
// var baseUrl="http://localhost:8000/";



function ajax_jobtitle()
{
	$.ajax({
		type:"GET",
		url:baseUrl+"api/all-job-titles",
		success:function(data)
		{
			create_selectoption(data);
			autocomplete_data(data);
		},
		error:function(err)
		{
			console.log("ERROR");
		}
	});

}

function datatable_jobtitle()
{

	$('#example1').DataTable( {
		retrieve: true,
		ajax:
		{
			url:baseUrl+"datatable_jobtitles",
			type:"GET"
		},
		columns: [
		{ data:"job_title"}

		]
	} );
}

function data_reload()
{
	var table=$('#example1').DataTable( {
		retrieve: true,
		ajax:
		{
					// url:baseUrl+"datatable_jobtitles",
					url:baseUrl+"datatable_jobtitles",
					type:"GET"
				},
				columns: [
				{ data:"job_title"}
				
				]} );

	table.ajax.reload();

	ajax_jobtitle();


}



function autocomplete_data(data)
{
	var auto_data=[];
	for(var i=0;i<data.length;i++)
	{
		var temp=data[i].job_title;
		auto_data[i]=temp;

	}
	$( ".autocom" ).autocomplete({
		source: auto_data
	});
}
function create_selectoption(data)
{
	$('#job_title').find('option').remove().end();
	$.each(data,function(i,value){
				// console.log(value.job_title);
				
				var op = $("<option></option>").text(value.job_title);
				op.attr("value",value.job_title);
				$('#job_title').append(op);
			});

}


$("#add").on("click",function()
{

	var title_data = $("#addin").val();
	$.ajax({
		type:"POST",
		url:baseUrl+"api/add-job-title",
		data: {
			title : title_data,
		},

		success:function(data)
		{

			data_reload();
			$("#addin").val("");
			console.log("Success");
					// console.log(data);
					// console.log(title_data);
					if(data == "jobtitle_yes")
					{
						
						new PNotify({
							title: 'Checked!',
							text: title_data+' already in job title table',
							type: 'info'
						});
						
					}
					else if(data == "job_title_blank")
					{
						
						new PNotify({
							title: 'Blank!',
							text: 'Blank Entery not Allowed',
							type: 'info'
						});

					}
					else
					{
						new PNotify({
							title: 'Success!',
							text: title_data+' added to job title table',
							type: 'success'
						});
					}



				},
				error:function(err)
				{
					console.log("ERROR");
				}
			});
});


$("#del").on("click",function()
{

	var title_data = $("#job_title").val();
	$.ajax({
		type:"POST",
		url:baseUrl+"api/remove-job-title",
		data: {
			title : title_data,
		},

		success:function(data)
		{
			data_reload();

			console.log("delete Success");
			new PNotify({
				title: 'Success!',
				text: title_data+' deleted from job title table',
				type: 'success'
			});
		},

		error:function(err)
		{
			console.log("ERROR");
		}
	});
});

$("#upd").on("click",function()
{
	var old_data = $("#job_title").val();
	var new_data = $("#updata").val();
	$.ajax({
		type:"POST",
		url:baseUrl+"api/rename-job-title",
		data: {
			old : old_data,
			new : new_data,
		},

		success:function(data)
		{
			data_reload();
			console.log(old_data);
			console.log(new_data);
			$("#updata").val("");
			console.log("update Success");
			if(data == "jobtitle_yes")
			{

				new PNotify({
					title: 'Error!',
					text: new_data+'already in job title table',
					type: 'error'
				});

			}
			else if(data == "job_title_blank")
			{

				new PNotify({
					title: 'Blank!',
					text: 'Blank Entery not Allowed',
					type: 'info'
				});

			}
			else
			{

				new PNotify({
					title: 'Success!',
					text: new_data+' added to job title table',
					type: 'success'
				});
			}


		},

		error:function(err)
		{
			console.log("Error"+JSON.stringify(err));
		}
	});
});
$("#logout").on("click",function()
{
	sessionStorage.setItem("checkk","invalid");
	$(location).attr('href','admin');

});