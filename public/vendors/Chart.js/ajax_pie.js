function barload()
{
var ctp = document.getElementById("mypie");
 var pieChartData=
        {
            labels:["may","june","july"],
            datasets:[
                    {
                        label: "No of Jobs",
                        data:[10,50,40],
                        backgroundColor:[
                        "rgba(32,178,170, 0.5)",
                        "rgba(255, 159, 64, 0.4)",
                        "rgba(153, 102, 255, 0.75)"
                        ],
                        borderColor:[
                        "rgba(32,178,170,1)",
                        "rgba(255, 159, 64,1)",
                        "rgba(153, 102, 255,1)"
                        ],
                       
                        borderWidth: [1,1,1]

                    }
                    ]
                };


 var pieChart = new Chart(ctp,
    {
        type:'pie',
        data:pieChartData,
        option:{}
    });
   
    
}



   