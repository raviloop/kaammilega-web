<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Kaammilega | </title>

  
  <!-- Bootstrap -->
  <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- iCheck -->
  <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

  <!-- bootstrap-progressbar -->
  <link href="/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">


  <!-- Custom Theme Style -->
  <link href="../build/css/custom.min.css" rel="stylesheet">
  <link href="/css_custom/css_custom_1.css" rel="stylesheet">
  <script>

    (function (){

     var result = sessionStorage.getItem("checkk");
     
     if( result != "admin")
     {
      location.href="unauthorized_access";
      
    }
    
  })();
</script>


</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="home" class="site_title"><div class="profile_pic"><img src="/production_files/images/56.png"/></div> <span>Kaammilega!</span></a>
          </div>

          <div class="clearfix"></div>

          <br />
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>Dashboard</h3>
              <ul class="nav side-menu">
                <li><a href="home"><i class="fa fa-bar-home"></i> Home </a></li>
                
                <li><a><i class="fa fa-home"></i> Adding <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                   <li><a href="adding_department"> Add Department </a></li>
                   <li><a href="adding_jobtitle"> Add Job Title </a></li>
                 </ul>
               </li>
               
               <li><a><i class="fa fa-home"></i> Retailers <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="all_retailer"> All Retailers </a></li>
                  <li><a href="all_retailer_job"> Retailers Posting </a></li>
                </ul>
              </li>
              <li><a><i class="fa fa-edit"></i> Salesmans <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="all_salesman"> All Salesmans </a></li>
                  <li><a href="all_salesman_job">Salesmans Job Applications</a></li>

                </ul>
              </li>
            </ul>
          </div>
        </div>
        

        <!-- /sidebar menu -->
      </div>
    </div>

    <!-- top navigation -->
    <div class="top_nav">
      <div class="nav_menu">
        <nav>
          <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
          </div>
          <ul class="nav navbar-nav navbar-right">
            <li class="">
              <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
               <img src="/production_files/images/user.png" alt="">Admin
               <span class=" fa fa-angle-down"></span>
             </a>
             <ul class="dropdown-menu dropdown-usermenu pull-right">
              <li ><a id="logout" href="#"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <!-- /top navigation -->


  <!-- page content -->
  <div class="right_col"  role="main">
    <!-- top tiles -->
    <div class="row tile_count">
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>  Retailers</span>
        <div id="retailer_header" class="count">  0</div>
        <span class="count_bottom"><i ><i id="1fa"></i><i id="retailer_per"> 0 </i></i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>  Salesman</span>
        <div id="salesman_header" class="count">  0</div>
        <span class="count_bottom"><i ><i id="2fa"></i><i id="salesman_per"> 0 </i></i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>  Applicants</span>
        <div id="applicant_header" class="count">  0</div>
        <span class="count_bottom"><i ><i id="3fa"></i><i id="applicant_per"> 0 </i></i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Jobs Posted</span>
        <div id="jobs_header" class="count">  0</div>
        <span class="count_bottom"><i ><i id="4fa"></i><i id="jobs_per"> 0 </i></i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Placements</span>
        <div id="placement_header" class="count">  0</div>
        <span class="count_bottom"><i ><i id="5fa"></i><i id="placement_per"> 0 </i></i> From last Week</span>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i>  Vacancies</span>
        <div id="vacancies_header" class="count">  0</div>
        <span class="count_bottom"><i ><i id="6fa"></i><i id="vacancies_per"> 0 </i></i> From last Week</span>
      </div>
    </div>
    <!-- /top tiles -->
    <div class="row">


      <div class="col-md-4 col-sm-4 col-xs-12">
        <div class="x_panel tile fixed_height_320">

          <div class="x_title">
            <h2>Most Posted Department Top 5</h2>
            <div class="clearfix"></div>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-6">
           <div style="margin-top: -5px;">
            <p id="jty0">Job Type</p>
            <div class="widget_summary">
              <div class="w_left w_25">
               <div class="progress progress_sm" style="width: 255%;">
                <div  id="jtyp0" class="progress-bar bg-green" role="progressbar" data-transitiongoal="90" style="width: 40%;"></div>
              </div>
            </div>
            <div class="w_right w_55">
              <p id="jtyc0">0</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div style="margin-top: -10px">
          <p id="jty1">Job Type</p>
          <div class="widget_summary">
            <div class="w_left w_25">
             <div class="progress progress_sm" style="width: 255%;">
              <div id="jtyp1" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
            </div>
          </div>
          <div class="w_right w_55">
            <p id="jtyc1"">0</p>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>

      <div style="margin-top:-10px">
        <p id="jty2">Job Type</p>
        <div class="widget_summary">
          <div class="w_left w_25">
           <div class="progress progress_sm" style="width: 255%;">
            <div id="jtyp2" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
          </div>
        </div>
        <div class="w_right w_55">
          <p id="jtyc2">0</p>
        </div>
        <div class="clearfix"></div>
      </div>

    </div>
    <div style="margin-top:-10px">
      <p id="jty3">Job Type</p>
      <div class="widget_summary">
        <div class="w_left w_25">
         <div class="progress progress_sm" style="width: 255%;">
          <div  id="jtyp3" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
        </div>
      </div>
      <div class="w_right w_55">
        <p id="jtyc3">0</p>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div style="margin-top:-10px">
    <p id="jty4">Job Type</p>
    <div class="widget_summary">
      <div class="w_left w_25">
       <div class="progress progress_sm" style="width: 255%;">
        <div id="jtyp4" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
      </div>
    </div>
    <div class="w_right w_55">
      <p id="jtyc4">0</p>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

</div>

</div>
</div>

<div class="col-md-4 col-sm-4 col-xs-12">
  <div class="x_panel tile fixed_height_320 overflow_hidden">
    <div class="x_title">
      <h2>Most Vacant Jobs Titles Top 5</h2>
      <div class="clearfix"></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-6">
     <div style="margin-top: -5px;">
      <p id="jt0">Job Titles</p>
      <div class="widget_summary">
        <div class="w_left w_25">
         <div class="progress progress_sm" style="width: 255%;">
          <div id="jtp0" class="progress-bar bg-green" data-transitiongoal="80" style="width: 40%;"></div>
        </div>
      </div>
      <div class="w_right w_55">
        <p id="jtc0">0</p>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div style="margin-top: -10px">
    <p id="jt1">Job Titles</p>
    <div class="widget_summary">
      <div class="w_left w_25">
       <div class="progress progress_sm" style="width: 255%;">
        <div id="jtp1" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
      </div>
    </div>
    <div class="w_right w_55">
      <p id="jtc1"">0</p>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

<div style="margin-top:-10px">
  <p id="jt2">Job Titles</p>
  <div class="widget_summary">
    <div class="w_left w_25">
     <div class="progress progress_sm" style="width: 255%;">
      <div id="jtp2" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
    </div>
  </div>
  <div class="w_right w_55">
    <p id="jtc2">0</p>
  </div>
  <div class="clearfix"></div>
</div>

</div>
<div style="margin-top:-10px">
  <p id="jt3">Job Titles</p>
  <div class="widget_summary">
    <div class="w_left w_25">
     <div class="progress progress_sm" style="width: 255%;">
      <div id="jtp3" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
    </div>
  </div>
  <div class="w_right w_55">
    <p id="jtc3">0</p>
  </div>
  <div class="clearfix"></div>
</div>
</div>
<div style="margin-top:-10px">
  <p id="jt4">Job Titles</p>
  <div class="widget_summary">
    <div class="w_left w_25">
     <div class="progress progress_sm" style="width: 255%;">
      <div  id="jtp4" class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" style="width: 40%;"></div>
    </div>
  </div>
  <div class="w_right w_55">
    <p id="jtc4">0</p>
  </div>
  <div class="clearfix"></div>
</div>
</div>

</div>
</div>
</div>

<div class="col-md-4 col-sm-4 col-xs-12">
  <div class="x_panel tile fixed_height_320 overflow_hidden">
    <div class="row x_title">
      <h2> Salary Basis </h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <canvas id="mypie1" width="500" height="400" ></canvas>
    </div>       
  </div>
</div>
</div>
</div>
<br/>

<!--Graph portion end-->

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="dashboard_graph">
      <div class="row x_title">
        <div class="col-md-6">
          <h3>Kaammilega Effectiveness</h3>
        </div>
      </div>
      <div class="col-md-12 col-sm-8 col-xs-12">
        <div class="canvas-container">
          <canvas id="mycanvas"></canvas>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>


  <!-- <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel tile fixed_height_320 overflow_hidden">
      <div class="row x_title">
        <h2>Salary Basis</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <canvas id="mypie1" width="500" height="400" ></canvas>
       </div>
     </div>
   </div>
 </div> -->
</div>

<!-- /page content -->

<!-- footer content -->
<footer>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
    All rights reserved . Crafted with <span>&hearts;</span> at <a href="http://centilliontech.in" target="new">Centillion</a>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<!-- jQuery -->
<script src="../vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- bootstrap-progressbar -->
<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- FastClick -->
<script src="../vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="../vendors/Chart.js/dist/Chart.js"></script>
<script src="../vendors/Chart.js/dist/Chart.min.js"></script>




<!-- gauge.js -->
<script src="../vendors/gauge.js/dist/gauge.min.js"></script>

<!-- iCheck -->
<script src="../vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="../vendors/skycons/skycons.js"></script>
<!-- Flot -->
<!--     <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
   <!--  <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <!-- <script src="../vendors/DateJS/build/date.js"></script> -->
    <!-- JQVMap -->
   <!--  <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script> -->
    <!-- bootstrap-daterangepicker -->
   <!--  <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script> -->

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script src="/panel_js/admin_panel.js"></script>
  </body>
  </html>
