<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>Kam Milega</title>		
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="/index_files/vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="/index_files/vendor/fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="/index_files/vendor/owlcarousel/owl.carousel.min.css" media="screen">
		<link rel="stylesheet" href="/index_files/vendor/owlcarousel/owl.theme.default.min.css" media="screen">
		<link rel="stylesheet" href="/index_files/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/index_files/css/theme.css">
		<link rel="stylesheet" href="/index_files/css/theme-elements.css">
		<link rel="stylesheet" href="/index_files/css/theme-blog.css">
		<link rel="stylesheet" href="/index_files/css/theme-shop.css">
		<link rel="stylesheet" href="/index_files/css/theme-animate.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="/index_files/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="/index_files/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/index_files/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="/index_files/css/custom.css">

		<!-- Head Libs -->
		<script src="/index_files/vendor/modernizr/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->

	</head>
	<body class="one-page" data-target=".single-menu" data-spy="scroll" data-offset="200">

		<div class="body">
			<header id="header" class="single-menu flat-menu">
				<div class="container">
					<div class="logo">
						<a href="#">
							<!-- <h1><b>kammilega.com</b></h1> -->
							<img src="/index_files/img/logo1.PNG"  class="size1">
						</a>
					</div>
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						<ul class="social-icons">
							<li class="facebook"><a href="#">Facebook</a></li>
							<li class="twitter"><a href="#">Twitter</a></li>
							<li class="linkedin"><a href="#">Linkedin</a></li>
							<li><a href="admin"><i class="fa fa-user fa-lg" aria-hidden="true"></i></a></li>
						</ul>
						<nav class="nav-main">
							<ul class="nav nav-pills nav-main" id="mainMenu">
								<li>
									<a href="index" style="color: #054153">Home page</a>
								</li>
								<li>
									<a data-hash href="#projects" style="color: #054153">Latest Jobs</a>
								</li>
								<!-- <li>
									<a data-hash href="#features">Features</a>
								</li> -->
								<li>
									<a data-hash href="#team" style="color: #054153">Meet the Team</a>
								</li>
								<li>
									<a data-hash href="#contact" style="color: #054153">Contact Us</a>
								</li>

							</ul>
						</nav>
					</div>
				</div>
			</header>


			<!-- code by dipesh -->
			<div class="slider-container">
					<div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 500}'>
						<ul>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
				
								<img src="/index_files/img/slides/slide-bg.jpg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
				
								<div class="tp-caption sft stb visible-lg"
									 data-x="177"
									 data-y="180"
									 data-speed="300"
									 data-start="1000"
									 data-easing="easeOutExpo"><img src="/index_files/img/slides/slide-title-border.png" alt=""></div>
				
								<div class="tp-caption top-label lfl stl"
									 data-x="227"
									 data-y="180"
									 data-speed="300"
									 data-start="500"
									 data-easing="easeOutExpo">DO YOU NEED A NEW</div>
				
								<div class="tp-caption sft stb visible-lg"
									 data-x="477"
									 data-y="180"
									 data-speed="300"
									 data-start="1000"
									 data-easing="easeOutExpo"><img src="/index_files/img/slides/slide-title-border.png" alt=""></div>
				
								<div class="tp-caption main-label sft stb"
									 data-x="135"
									 data-y="210"
									 data-speed="300"
									 data-start="1500"
									 data-easing="easeOutExpo">Job Resource?</div>
				
								<div class="tp-caption bottom-label sft stb"
									 data-x="185"
									 data-y="280"
									 data-speed="500"
									 data-start="2000"
									 data-easing="easeOutExpo">Check out our options and features.</div>
				
								<div class="tp-caption randomrotate"
									 data-x="905"
									 data-y="248"
									 data-speed="500"
									 data-start="2500"
									 data-easing="easeOutBack"><img src="/index_files/img/slides/slide-concept-2-1.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="955"
									 data-y="200"
									 data-speed="400"
									 data-start="3000"
									 data-easing="easeOutBack"><img src="/index_files/img/slides/slide-concept-2-2.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="925"
									 data-y="170"
									 data-speed="700"
									 data-start="3150"
									 data-easing="easeOutBack"><img src="/index_files/img/slides/slide-concept-2-3.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="875"
									 data-y="130"
									 data-speed="1000"
									 data-start="3250"
									 data-easing="easeOutBack"><img src="/index_files/img/slides/slide-concept-2-4.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="605"
									 data-y="80"
									 data-speed="600"
									 data-start="3450"
									 data-easing="easeOutExpo"><img src="/index_files/img/slides/slide-concept-2-5.png" alt=""></div>
				
								<div class="tp-caption blackboard-text lfb "
									 data-x="635"
									 data-y="300"
									 data-speed="500"
									 data-start="3450"
									 data-easing="easeOutExpo" style="font-size: 37px;"></div>
				
								<div class="tp-caption blackboard-text lfb "
									 data-x="660"
									 data-y="350"
									 data-speed="500"
									 data-start="3650"
									 data-easing="easeOutExpo" style="font-size: 47px;"></div>
				
								<div class="tp-caption blackboard-text lfb "
									 data-x="685"
									 data-y="400"
									 data-speed="500"
									 data-start="3850"
									 data-easing="easeOutExpo" style="font-size: 32px;"></div>
							</li>
							<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" >
				
								<img src="/index_files/img/slides/slide-bg.jpg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
				
									<div class="tp-caption sft stb"
										 data-x="155"
										 data-y="100"
										 data-speed="600"
										 data-start="100"
										 data-easing="easeOutExpo"><img src="/index_files/img/slides/slide-concept.png" alt=""></div>
				
									<div class="tp-caption blackboard-text sft stb"
										 data-x="285"
										 data-y="180"
										 data-speed="900"
										 data-start="1000"
										 data-easing="easeOutExpo" style="font-size: 30px;">easy to</div>
				
									<div class="tp-caption blackboard-text sft stb"
										 data-x="285"
										 data-y="220"
										 data-speed="900"
										 data-start="1200"
										 data-easing="easeOutExpo" style="font-size: 40px;">find the job!</div>
				
									<div class="tp-caption main-label sft stb"
										 data-x="685"
										 data-y="190"
										 data-speed="300"
										 data-start="900"
										 data-easing="easeOutExpo">FIND IT!</div>
				
									<div class="tp-caption bottom-label sft stb"
										 data-x="685"
										 data-y="250"
										 data-speed="500"
										 data-start="2000"
										 data-easing="easeOutExpo"></div>
				
							</li>
						</ul>
					</div>
				</div>
			<!-- code by dipesh -->


				<section class=" color1 ">
					<div class="container color1">
						<div class="row color1" id="projects">
							<div class="col-md-12">
								<h2 style="color: #fff">Latest <strong>Jobs</strong></h2>

								<div class="owl-carousel owl-carousel-spaced" data-plugin-options='{"items": 4}'>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/stock.jpg">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">SEO</span>
													<span class="thumb-info-type">Website</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject" class="popup-inline-content">
											<h2>Web Designer</h2>

											<div class="row">
												<div class="col-md-6">
													<!-- <img class="img-thumbnail img-responsive" alt="" src="img/projects/project.jpg"> -->
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/photo.jpeg">
												</div>
												<div class="col-md-6">

													<h4><strong>Web Designer</strong></h4>
													<p> Design award-worthy web/mobile sites, mobile apps and other digital media with creative flair.Keep up to date with current design trends, UX considerations and web/app design innovation.Coordinate with technology teams, production houses and agencies to oversee the execution of your designs through to final delivery.Collaborate in creative direction for other media such as video, print and audio from time to time.
														<br>
														<h4><b>You'll need to have</b></h4>
														<ol>
															<li>Excellent English communication skills</li>
															<li>At least 3 years of experience designing travel, ecommerce or data-driven websites/apps</li>
															<li>A degree in visual design or an exceptional portfolio</li></ol>



													</p>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>

													<!-- <h4 class="push-top">Services</h4>

													 <ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->
 
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject2" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/mob1.jpg">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">Okler</span>
													<span class="thumb-info-type">Brand</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject2" class="popup-inline-content">
											<h2>Mobile Developer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/mob.jpg">
												</div>
												<div class="col-md-6">

													<h4><strong>Mobile Developer</strong></h4>
													<p>Design and build advanced applications for the Android /iOS platform using native /middleware technologies.Collaborate with cross-functional teams to define, design, and ship new features.Work with outside data sources and APIs.Unit-test code for robustness, including edge cases, usability, and reliability.Continuously discover, evaluate, and implement new technologies to maximize development efficiency.</p>
													<br>
													<h4><b>You'll need to have</b></h4>
														<ol>
															<li>2+ Years of development experience in Android /iOS.</li>
															<li>Have published one or more Android/iOS apps in the app store.</li>
															<li>A deep familiarity with Android SDK, Rest, JSON or Objective-C, Cocoa Touch, Core Data, and Swift.</li>
															<li>Solid understanding of the full mobile development life cycle.</li></ol>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>

													<!-- <h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->

												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject3" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/uiux.png" height="200px">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">The Fly</span>
													<span class="thumb-info-type">Logo</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject3" class="popup-inline-content">
											<h2>UI/UX Designer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/uiux1.jpeg">
												</div>
												<div class="col-md-6">

													<h4><strong>UI/UX Designer</strong></h4>
													<p>Design innovative and appealing experiences on a rapid and iterative basis while planning for the direction of future iterations.Advocate for your design solutions by putting them in context of business and user goals.Actively participate in team brainstorming and solution generation through quick sketches or low fidelity wireframes.Develop high level and/or detailed storyboards, mockups, and prototypes to effectively communicate interaction and design ideas.</p>
													<br>
													<h4><b>You'll need to have</b></h4>
														<ol>
															<li>2-4 years' experience working as a UX/Interaction Designer, ideally for consumer web and mobile products Deep understanding of design methodologies and principles</li>
															<li>Ability to understand and analyze user needs and conduct user testings and surveys</li>
															<li> Understanding of HTML and CSS</li>
															<li> Should have Masters or bachelors in design from premier school.</li></ol>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>

													<!-- <h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->

												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject4" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/developer.jpeg">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">The Code</span>
													<span class="thumb-info-type">Website</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject4" class="popup-inline-content">
											<h2>Software Developer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/software1.jpeg">
												</div>
												<div class="col-md-6">

													<h4><strong>Software Developer</strong></h4>
													<p>We are now looking for a Software Developer that will contribute to the creation of customer solutions. As a Software Developer, you will be involved in all phases of the software development lifecycle; working towards properly engineered information systems, containing software as the major component, to meet agreed business requirements.</p>
													<br>
													<h4><b>You'll need to have</b></h4>
														<ol>
															<li>3-5 years experience of deploy system test and lead testing team.</li>
															<li>Good knowledge in Object Oriented Design and development 2 year experience in java.</li>
															<li> Experienced in Designing and developing high-volume, low-latency applications for mission-critical systems and delivering high-availability and performance</li>
															</ol>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>

													<!-- <h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->

												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject5" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/stock.jpg">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">SEO</span>
													<span class="thumb-info-type">Website</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject5" class="popup-inline-content">
											<h2>Web Designer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/photo.jpeg">
												</div>
												<div class="col-md-6">

													<h4><strong>Web Designer (Junior)</strong></h4>
													<p> Design award-worthy web/mobile sites, mobile apps and other digital media with creative flair.Keep up to date with current design trends, UX considerations and web/app design innovation.Coordinate with technology teams, production houses and agencies to oversee the execution of your designs through to final delivery.Collaborate in creative direction for other media such as video, print and audio from time to time.
														<br>
														<h4><b>You'll need to have</b></h4>
														<ol>
															<li>Excellent English communication skills</li>
															<li>At least 1 years of experience designing travel, ecommerce or data-driven websites/apps</li>
															<li>A degree in visual design or an exceptional portfolio</li></ol>



													</p>

													<!-- <a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>

													<h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul>
 -->
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject6" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/mob1.jpg">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">Okler</span>
													<span class="thumb-info-type">Brand</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject6" class="popup-inline-content">
											<h2>Mobile Developer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/mob.png">
												</div>
												<div class="col-md-6">

													<h4><strong>Mobile Developer (Junior)</strong></h4>
													<p>Design and build advanced applications for the Android /iOS platform using native /middleware technologies.Collaborate with cross-functional teams to define, design, and ship new features.Work with outside data sources and APIs.Unit-test code for robustness, including edge cases, usability, and reliability.Continuously discover, evaluate, and implement new technologies to maximize development efficiency.</p>
													<br>
													<h4><b>You'll need to have</b></h4>
														<ol>
															<li>1 Years of development experience in Android /iOS.</li>
															<li>Have published one or more Android/iOS apps in the app store.</li>
															<li>A deep familiarity with Android SDK, Rest, JSON or Objective-C, Cocoa Touch, Core Data, and Swift.</li>
															<li>Solid understanding of the full mobile development life cycle.</li></ol>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>


													<!-- <h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->

												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject3" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/uiux.png">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">The Fly</span>
													<span class="thumb-info-type">Logo</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject3" class="popup-inline-content">
											<h2>UI/UX Designer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/uiux1.jpeg">
												</div>
												<div class="col-md-6">

													<h4><strong>UI/UX Designer(Junior)</strong></h4>
													<p>Design innovative and appealing experiences on a rapid and iterative basis while planning for the direction of future iterations.Advocate for your design solutions by putting them in context of business and user goals.Actively participate in team brainstorming and solution generation through quick sketches or low fidelity wireframes.Develop high level and/or detailed storyboards, mockups, and prototypes to effectively communicate interaction and design ideas.</p>
													<br>
													<h4><b>You'll need to have</b></h4>
														<ol>
															<li>1 years' experience working as a UX/Interaction Designer, ideally for consumer web and mobile products Deep understanding of design methodologies and principles</li>
															<li>Ability to understand and analyze user needs and conduct user testings and surveys</li>
															<li> Understanding of HTML and CSS</li>
															<li> Should have Masters or bachelors in design from premier school.</li></ol>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>


<!-- 													<h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->

												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="portfolio-item img-thumbnail">
											<a class="thumb-info lightbox" href="#popupProject4" data-plugin-options='{"type":"inline", preloader: false}'>
												<img alt="" class="img-responsive size" src="/index_files/img/developer.jpeg">
												<!-- <span class="thumb-info-title">
													<span class="thumb-info-inner">The Code</span>
													<span class="thumb-info-type">Website</span>
												</span> -->
												<span class="thumb-info-action">
													<span title="Universal" class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</a>
										</div>
										<div id="popupProject4" class="popup-inline-content">
											<h2>Software Developer</h2>

											<div class="row">
												<div class="col-md-6">
													<img class="img-thumbnail img-responsive" alt="" src="/index_files/img/software1.jpeg">
												</div>
												<div class="col-md-6">

													<h4><strong>Software Developer (Junior)</strong></h4>
													<p>We are now looking for a Software Developer that will contribute to the creation of customer solutions. As a Software Developer, you will be involved in all phases of the software development lifecycle; working towards properly engineered information systems, containing software as the major component, to meet agreed business requirements.</p>
													<br>
													<h4><b>You'll need to have</b></h4>
														<ol>
															<li>3 years experience of deploy system test and lead testing team.</li>
															<li>Good knowledge in Object Oriented Design and development 1 year experience in java.</li>
															<li> Experienced in Designing and developing high-volume, low-latency applications for mission-critical systems and delivering high-availability and performance</li>
															</ol>

													<a href="#" class="btn btn-primary">Apply Now</a> <span class="arrow hlb"></span>
<!-- 
													<h4 class="push-top">Services</h4>

													<ul class="list icons list-unstyled">
														<li><i class="fa fa-check"></i> Design</li>
														<li><i class="fa fa-check"></i> HTML/CSS</li>
														<li><i class="fa fa-check"></i> Javascript</li>
														<li><i class="fa fa-check"></i> Backend</li>
													</ul> -->

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> 
				<br><br>
				<div class="container">

					<div class="row center">
						<div class="col-md-12">
							<h1 class="short word-rotator-title">
								Find the right job on <strong>kammilega.com </strong>
								<!-- <strong class="inverted" data-appear-animation="bounceIn">
									<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
										<span class="word-rotate-items">
											<span>incredibly</span>
											<span>especially</span>
											<span>extremely</span>
										</span>
									</span>
								</strong>
								very useful for searching --></h1>
							<p class="featured lead">
								You are only few steps away from millions of jobs
							</p>
						</div>
					</div>

					<hr class="tall" />
				</div>

				<!-- <div class="container">
					<div class="row" id="features">
						<div class="col-md-8">
							<h2 data-appear-animation="fadeInLeft">Our <strong>Features</strong></h2>
							<div class="row">
								<div class="col-sm-6">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-group"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Customer Support</h4>
											<p class="tall">Lorem ipsum dolor sit amet, consectetur adip.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-file"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">HTML5 / CSS3 / JS</h4>
											<p class="tall">Lorem ipsum dolor sit amet, adip.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-google-plus"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">500+ Google Fonts</h4>
											<p class="tall">Lorem ipsum dolor sit amet, consectetur adip.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-adjust"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Colors</h4>
											<p class="tall">Lorem ipsum dolor sit amet, consectetur adip.</p>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-film"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Sliders</h4>
											<p class="tall">Lorem ipsum dolor sit amet, consectetur.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-user"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Icons</h4>
											<p class="tall">Lorem ipsum dolor sit amet, consectetur adip.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-bars"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Buttons</h4>
											<p class="tall">Lorem ipsum dolor sit, consectetur adip.</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-desktop"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Lightbox</h4>
											<p class="tall">Lorem sit amet, consectetur adip.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<h2 data-appear-animation="fadeInRight">and more...</h2>

							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
												<i class="fa fa-usd"></i>
												Pricing Tables
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="accordion-body collapse in">
										<div class="panel-body">
											Donec tellus massa, tristique sit amet condim vel, facilisis quis sapien. Praesent id enim sit amet odio vulputate eleifend in in tortor.
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
												<i class="fa fa-comment"></i>
												Contact Forms
											</a>
										</h4>
									</div>
									<div id="collapseTwo" class="accordion-body collapse">
										<div class="panel-body">
											Donec tellus massa, tristique sit amet condimentum vel, facilisis quis sapien.
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
												<i class="fa fa-laptop"></i>
												Portfolio Pages
											</a>
										</h4>
									</div>
									<div id="collapseThree" class="accordion-body collapse">
										<div class="panel-body">
											Donec tellus massa, tristique sit amet condimentum vel, facilisis quis sapien.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
 -->
				<section class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(/index_files/img/parallax.jpg);">
					<div class="container">
						<div class="row center">
							<div class="col-md-12">
								<i class="fa fa-comments icon-featured" data-appear-animation="bounceIn"></i>
								<h2 class="short text-shadow big white bold"><strong>We're excited about Kammilega.com</strong></h2>
								<!-- <h3 class="lead white">5,500 customers in 100 countries use Porto Template. Meet our customers.</h3> -->
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row" id="team">
						<div class="col-md-12">
							<h2 data-appear-animation="fadeInLeft">Meet the <strong style="color: #f59e0e">Team</strong></h2>

							<div class="row">

								<ul class="team-list">
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="#" class="thumb-info team">
												<img alt="" height="270" src="/index_files/img/pic01.jpg">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">John Doe</span>
													<span class="thumb-info-type">CEO</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan.</p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom"  href="#" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="#" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="#" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="#" class="thumb-info team">
												<img alt="" height="270" src="/index_files/img/pic02.jpg">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">Jessica Doe</span>
													<span class="thumb-info-type">Marketing</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan.</p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom"  href="#" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="#" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="#" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="#" class="thumb-info team">
												<img alt="" height="270" src="/index_files/img/pic03.jpg">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">Rick Edward Doe</span>
													<span class="thumb-info-type">Developer</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan.</p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom"  href="#" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="#" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="#" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="#" class="thumb-info team">
												<img alt="" height="270" src="/index_files/img/pic04.jpg">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">Melinda Wolosky</span>
													<span class="thumb-info-type">Design</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan.</p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
								</ul>

							</div>
						</div>
					</div>
				</div>

				<section class="parallax" data-stellar-background-ratio="0.5" style="background-image: url(/index_files/img/parallax-transparent.jpg);">
					<div class="container">
						<div class="row center">
							<div class="col-md-12">
								<i class="fa fa-envelope icon-featured" data-appear-animation="bounceIn"></i>
								<h2 class="short text-shadow big bold" data-appear-animation="fadeInUp"><strong>Get in Touch With Us</strong></h2>
							</div>
						</div>
					</div>
				</section>

				<!-- Google Maps -->
				<!-- <div id="googlemaps" class="google-map push-top"></div> -->
				<div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.086946749336!2d73.0351449499878!3d19.015889987059236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c3ac57500013%3A0xd33dd1a23d83b664!2sAmexs+Technologies!5e0!3m2!1sen!2sin!4v1499248988279" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe></div>

				<div class="container">

					<div class="row" id="contact">
						<div class="col-md-6">

							<div class="alert alert-success hidden" id="contactSuccess">
								<strong>Success!</strong> Your message has been sent to us.
							</div>

							<div class="alert alert-danger hidden" id="contactError">
								<strong>Error!</strong> There was an error sending your message.
							</div>

							<h2 class="short" style="color: #172936"><strong style="color: #054153">Contact</strong> Us</h2>
							<form id="contactForm" action="php/contact-form.php" method="POST">
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label>Your name *</label>
											<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
										</div>
										<div class="col-md-6">
											<label>Your email address *</label>
											<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Subject</label>
											<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Message *</label>
											<textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message" required></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input type="submit" value="Send Message" class="btn btn-primary btn-lg" data-loading-text="Loading..." style="background-color: #172936">
									</div>
								</div>
							</form>
						</div>
						<div class="col-md-6">

							<h4 class="push-top" style="color: #054153">Get in <strong style="color: #172936">touch</strong></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus.</p>

							<hr />

							<h4 style="color: #054153">The <strong style="color: #172936">Office</strong></h4>
							<ul class="list-unstyled">
								<li><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</li>
								<li><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</li>
								<li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
							</ul>

							<hr />

							<h4 style="color: #054153">Business <strong style="color: #172936">Hours</strong></h4>
							<ul class="list-unstyled">
								<li><i class="fa fa-time"></i> Monday - Friday 9am to 5pm</li>
								<li><i class="fa fa-time"></i> Saturday - 9am to 2pm</li>
								<li><i class="fa fa-time"></i> Sunday - Closed</li>
							</ul>

						</div>

					</div>

				</div>
			</div>

			<footer class="short" id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<h4>About Kammilega.com</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Phasellus semper scelerisque purus, et semper nisl lacinia sit amet. Praesent venenatis turpis vitae purus semper, eget sagittis velit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos...</p>
							<hr class="light">
						</div>
						<div class="col-md-3 col-md-offset-1">
							<h5 class="short">Contact Us</h5>
							<span class="phone">(800) 123-4567</span>
							<p class="short">International: (333) 456-6670</p>
							<p class="short">Fax: (222) 531-8999</p>
							<ul class="list icons list-unstyled">
								<li><i class="fa fa-envelope"></i> <a href="mailto:okler@okler.net">okler@okler.net</a></li>
							</ul>
							<div class="social-icons">
								<ul class="social-icons">
									<li class="facebook"><a href="#" data-placement="bottom" data-tooltip title="Facebook">Facebook</a></li>
									<li class="twitter"><a href="#" data-placement="bottom" data-tooltip title="Twitter">Twitter</a></li>
									<li class="linkedin"><a href="#" data-placement="bottom" data-tooltip title="Linkedin">Linkedin</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-1">
								<a href="index.php" class="logo">
									<!-- <h4>Kammilega.com</h4> -->
									<img src="/index_files/img/logo1.PNG"  class="size1">
								</a>
								<br>
							</div>
							<div class="col-md-11">
								<br>
								<div class="padding">
								<p>© Copyright 2017 by Amexstech.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="/index_files/vendor/jquery/jquery.js"></script>
		<script src="/index_files/vendor/jquery.appear/jquery.appear.js"></script>
		<script src="/index_files/vendor/jquery.easing/jquery.easing.js"></script>
		<script src="/index_files/vendor/jquery-cookie/jquery-cookie.js"></script>
		<script src="/index_files/vendor/bootstrap/bootstrap.js"></script>
		<script src="/index_files/vendor/common/common.js"></script>
		<script src="/index_files/vendor/jquery.validation/jquery.validation.js"></script>
		<script src="/index_files/vendor/jquery.stellar/jquery.stellar.js"></script>
		<script src="/index_files/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="/index_files/vendor/jquery.gmap/jquery.gmap.js"></script>
		<script src="/index_files/vendor/isotope/jquery.isotope.js"></script>
		<script src="/index_files/vendor/owlcarousel/owl.carousel.js"></script>
		<script src="/index_files/vendor/jflickrfeed/jflickrfeed.js"></script>
		<script src="/index_files/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="/index_files/vendor/vide/vide.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="/index_files/js/theme.js"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script src="/index_files/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="/index_files/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="/index_files/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="/index_files/js/views/view.home.js"></script>
		<script src="/index_files/js/views/view.contact.js"></script>
		
		<!-- Theme Custom -->
		<script src="/index_files/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="/index_files/js/theme.init.js"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script type="text/javascript">
		
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-12345678-1']);
			_gaq.push(['_trackPageview']);
		
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		
		</script>
		 -->

		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script>

			/*
			Map Settings

				Find the Latitude and Longitude of your address:
					- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
					- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

			*/

			// Map Markers
			var mapMarkers = [{
				address: "217 Summit Boulevard, Birmingham, AL 35243",
				html: "<strong>Alabama Office</strong><br>217 Summit Boulevard, Birmingham, AL 35243<br><br><a href='#' onclick='mapCenterAt({latitude: 33.44792, longitude: -86.72963, zoom: 16}, event)'>[+] zoom here</a>",
				icon: {
					image: "img/pin.png",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			},{
				address: "645 E. Shaw Avenue, Fresno, CA 93710",
				html: "<strong>California Office</strong><br>645 E. Shaw Avenue, Fresno, CA 93710<br><br><a href='#' onclick='mapCenterAt({latitude: 36.80948, longitude: -119.77598, zoom: 16}, event)'>[+] zoom here</a>",
				icon: {
					image: "img/pin.png",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			},{
				address: "New York, NY 10017",
				html: "<strong>New York Office</strong><br>New York, NY 10017<br><br><a href='#' onclick='mapCenterAt({latitude: 40.75198, longitude: -73.96978, zoom: 16}, event)'>[+] zoom here</a>",
				icon: {
					image: "img/pin.png",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				}
			}];

			// Map Initial Location
			var initLatitude = 37.09024;
			var initLongitude = -95.71289;

			// Map Extended Settings
			var mapSettings = {
				controls: {
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: true,
					streetViewControl: true,
					overviewMapControl: true
				},
				scrollwheel: false,
				markers: mapMarkers,
				latitude: initLatitude,
				longitude: initLongitude,
				zoom: 5
			};

			var map = $("#googlemaps").gMap(mapSettings);

			// Map Center At
			var mapCenterAt = function(options, e) {
				e.preventDefault();
				$("#googlemaps").gMap("centerAt", options);
			}

		</script>

	</body>
</html>
