<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Kaammilega | </title>

  <!-- Bootstrap -->
  <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- NProgress -->
  <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
  <!-- Animate.css -->
  <link href="/vendors/animate.css/animate.min.css" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="/build/css/custom.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/css_custom/css_custom_1.css">
</head>

<body class="login">
 <div class="login_wrapper">
  <section class="login_content">
    <form id="form1" method="POST">
      <div class="text-center">
       <img src="/production_files/images/icon2.png" width="120" height="102"/>
       <h5 class="content-group">Login to your account </h5>
     </div>


     
     <input type="text" class="form-control" placeholder="Username" name="user" required="required">
     <input type="password" class="form-control" placeholder="Password" name="password" required="required">
     
     
     <!-- <a class="btn btn-default btn-red btn-block submit" >Login</a> -->
     <input style="margin-left:0px;" type="submit" name="submit" class="btn btn-default btn-red btn-block submit">

     <div id="error"></div>

     <div class="clearfix"></div>
   </form>
 </section>
 
</div>



<!-- jQuery -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<script src="/panel_js/login_ajax.js"></script>
<footer id="log_footer">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"">
    All rights reserved . Crafted with <span >&hearts;</span> at <a href="http://centilliontech.in" target="new">Centillion</a>
  </div>
  <div class="clearfix"></div>
</footer>
</body>
</html>
